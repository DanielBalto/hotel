<?php

include_once 'model/ModelEmpleado.php';

class RestaurarController{
    
    private $empleado;
    
    
    public function __CONSTRUCT() {
        
        $this->empleado = new Empleado();
    }
    
    
    public function backup(){
        $db = new Empleado();
        $db->restaurar();
        header('Location: index.php?c=Empleado');
    }
    
    
}

