<?php

class InicioController{
    
   
    
    public function __CONSTRUCT(){
        
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/informativo/inicio.php';
        require_once 'view/footer.php';
    }
    public function CerrarSession(){
        session_start();
        session_destroy();
        require_once 'view/header.php';
        require_once 'view/informativo/inicio.php';
        require_once 'view/footer.php';
    }
 
    
}