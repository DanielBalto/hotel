<?php
include_once 'model/ModelReserva.php';
include_once 'model/ModelCliente.php';
include_once 'controller/reservas.controller.php';


class ReservadminController{
    
     private $reservaController;
     private $id;
     private $reserva;

    public function __CONSTRUCT() {
        $this->reservaController = new ReservasController();
        $this->id = new Cliente();
        $this->reserva = new Reserva();
    }
    
    public function idCliente($id){
       
        return $this->id = $id;
    }

    
    public function Index(){
        require_once 'view/headerAdmin.php';
        require_once 'view/reservas/ingresar-reserva.php';
        require_once 'view/footerAdmin.php';
    }
    
    
    public function ReservaCliente(){
        $cli = new Cliente();
        
        if(isset($_REQUEST['idCliente'])){
        $cli->idCliente = $_REQUEST['idCliente'];
        $cli->nombre = $_REQUEST['nombre'];
          
        $this->id = $this->reservaController->idCliente($cli);
        
        $this->reserva->idReserva = $this->reserva->ObtenerIdReserva();
        }
        require_once 'view/headerLogin.php';
        require_once 'view/reservas/ingresar-reserva.php';
        require_once 'view/footerAdmin.php';
    }
    
 
    
}