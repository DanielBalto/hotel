<?php

include_once 'model/ModelServicio.php';

class ServicioController {

    private $model;

    public function __CONSTRUCT() {
        $this->model = new Servicio();
    }

    public function Index() {
        require_once 'view/headerAdmin.php';
        require_once 'view/servicios/servicio.php';
        require_once 'view/footerAdmin.php';
    }

    public function Editar() {
        $usua = new Servicio();

        if (isset($_REQUEST['id'])) {
            $usua = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/servicios/servicio-editar.php';
        require_once 'view/footerAdmin.php';
    }

    public function Registrar() {
        $usua = new Servicio();

        if (isset($_REQUEST['idServicio'])) {
            $usua = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/servicios/servicio-editar.php';
        require_once 'view/footerAdmin.php';
    }

    public function Guardar() {
        $serv = new Servicio();

       
        $serv->descripcion = $_REQUEST['Descripcion'];
        $serv->costo = $_REQUEST['Costo'];
        $serv->estado = $_REQUEST['Estado'];
        $serv->Registrar($serv);       

        header('Location: index.php?c=Servicio');
    }

    public function Eliminar() {
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php?c=Servicio');
    }

}
