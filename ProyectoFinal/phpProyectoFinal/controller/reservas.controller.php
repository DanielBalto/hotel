<?php
include_once 'model/ModelHabitacion.php';
include_once 'model/ModelReserva.php';
include_once 'model/ModelCliente.php';

class ReservasController{
    
  private $reser;
  private $hab;
  
  private $cliente;
    public function __CONSTRUCT(){
        $this->hab = new Habitacion();
        $this->reser = new Reserva();
        $this->cliente = new Cliente();
        
    }
    
    public function idCliente($id){
       
        return $this->cliente = $id;
    }


    public function Index(){
        $cliente = new Cliente();
        $cliente = $this->cliente;
        require_once 'view/header.php';
        require_once 'view/informativo/reservas.php';
        require_once 'view/footer.php';
    }
    
    public function IndexCliente(){
        require_once 'view/header.php';
        require_once 'view/cliente/registro-cliente.php';
        require_once 'view/footer.php';
    }
    
 
    public function ClienteGuardar() {
        $clien = new Cliente();

        $clien->idCliente = $_REQUEST['id'];
        $clien->nombre = $_REQUEST['Nombre'];
        $clien->apellido = $_REQUEST['Apellido'];
        $clien->telefono = $_REQUEST['Telefono'];
        $clien->correo = $_REQUEST['Correo'];
        $clien->Registrar($clien);
        //$this->model->Registrar($clien);

    }
    
    public function ReservaGuardar(){
        $reser = new Reserva();
        
        
        $reser->idCliente = $_REQUEST['idCliente'];
        $reser->fecha = $_REQUEST['fechaPago'];
        $reser->cantAdultos =$_REQUEST['cantAdultos'];
        $reser->cantNinos = $_REQUEST['cantNinos'];       
        $reser->numTarjeta = $_REQUEST['numTarjeta'];
        $reser->fechaCaducTarj = $_REQUEST['fechaCaduc'];
        $reser->fechaIngreso = $_REQUEST['fechaIngreso'];
        $reser->fechaSalida = $_REQUEST['fechaSalida'];
        //Subtotal no lo envio pero en el registrar esta enviado manual 0.0;
        
        $reser->RegistrarReserva($reser);
        
        if($_POST['habitaciones'] != ""){
            if(is_array($_POST['habitaciones'])){
                while(list($key,$value) = each($_POST['habitaciones'])){
                   
                   $reser->idReserva = $_REQUEST['idReserva'];
                   $reser->idHabitacion = $value;
                   $reser->DetalleReserva($reser);
                }
            }
            
        }
        
        //CREAR IF PARA SERVICIOS CON CHECK LIST
        
        if($_POST['servicios'] != ""){
            if(is_array($_POST['servicios'])){
                while(list($key,$value) = each($_POST['servicios'])){
                   
                   $reser->idReserva = $_REQUEST['idReserva'];
                   $reser->reservaIDServ = $value;
                   $reser->ReservaServicio($reser);
                }
            }
            
        }
        
    }
    
    public function Guardar() {
        $this->ReservaGuardar();
        header('Location: index.php?c=inicio');
    }
    
}