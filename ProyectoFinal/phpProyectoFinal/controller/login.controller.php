<?php

include_once 'model/ModelEmpleado.php';

class LoginController {

    private $model;

    public function __CONSTRUCT() {
        $this->model = new Empleado();
    }

    public function Index() {
        require_once 'view/headerLogin.php';
        require_once 'view/login/login.php';
        require_once 'view/footerAdmin.php';
    }

    public function Autenticar() {

        $nombre = $_REQUEST['NombreUsuario'];
        $password = $_REQUEST[('Clave')];
        $validar = $this->model->Verificar($nombre, sha1($password));
        if ($validar) {
            session_start();
            
            $_SESSION['user'] = $_REQUEST['NombreUsuario'];
            $sesion = $_SESSION['user'];
            header('Location: index.php?c=Empleado');
        } else {

            header('Location: index.php?c=Login&error=true');
            // echo "<script language='JavaScript'>alert('Usuar');</script>";
        }
    }

}
