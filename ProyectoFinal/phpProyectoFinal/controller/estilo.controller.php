<?php

include_once 'model/ModelEstilo.php';

class EstiloController {

    private $model;

    public function __CONSTRUCT() {
        $this->model = new Estilo();
    }

    public function Index() {
        require_once 'view/headerAdmin.php';
        require_once 'view/estilos/estilos.php';
        require_once 'view/footerAdmin.php';
    }

    public function Editar() {
        $estilo = new Estilo();

        if (isset($_REQUEST['id'])) {
            $estilo = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/estilos/estilo-editar.php';
        require_once 'view/footerAdmin.php';
    }

    public function Registrar() {
        $estilo = new Estilo();

        if (isset($_REQUEST['idEstilo'])) {
            $estilo = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/estilos/estilo-editar.php';
        require_once 'view/footerAdmin.php';
    }

    public function Guardar() {
        $estilo = new Estilo();

        $estilo->descripcion = $_REQUEST['Descripcion'];

        $estilo->Registrar($estilo);

        header('Location: index.php?c=Estilo');
    }

    public function Eliminar() {
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php?c=Estilo');
    }

}
