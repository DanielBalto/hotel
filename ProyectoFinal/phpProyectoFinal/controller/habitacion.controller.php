<?php

include_once 'model/ModelHabitacion.php';
include_once 'model/ModelMostrar.php';

class HabitacionController {

    private $hab;
    private $model;

    public function __CONSTRUCT() {
        $this->hab = new Habitacion();
        $this->model = new Mostrar();
    }

    public function Index() {
        require_once 'view/headerAdmin.php';
        require_once 'view/habitacion/habitacion.php';
        require_once 'view/footerAdmin.php';
    }

    public function Editar() {
        $hab = new Habitacion();

        if (isset($_REQUEST['id'])) {
            $hab = $this->hab->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/habitacion/habitacion-editar.php';
        require_once 'view/footerAdmin.php';
    }

    public function Registrar() {
        $usua = new Habitacion();

        if (isset($_REQUEST['idHabitacion'])) {
            $usua = $this->hab->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/habitacion/habitacion-editar.php';
        require_once 'view/footer.php';
    }

    public function Guardar() {
        $hab = new Habitacion();

        $hab->idHabitacion = $_REQUEST['NumHab'];
        $hab->costo = $_REQUEST['Costo'];
        $hab->capacidad = $_REQUEST['Capacidad'];
        $hab->estado = $_REQUEST['Estado'];
        $hab->estilo = $_REQUEST['Estilo'];
        $hab->Registrar($hab);

        header('Location: index.php?c=Habitacion');
    }

    public function Eliminar() {
        $this->hab->Eliminar($_REQUEST['id']);
        header('Location: index.php?c=Habitacion');
    }

}
