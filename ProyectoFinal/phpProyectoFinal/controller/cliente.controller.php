<?php

include_once 'model/ModelCliente.php';
include_once 'controller/reservadmin.controller.php';

class ClienteController {

    private $model;
    private $reservadmin;

    public function __CONSTRUCT() {
        $this->model = new Cliente();
        $this->reservadmin = new ReservadminController();
    }
    

    public function Index() {
        require_once 'view/headerAdmin.php';
        require_once 'view/cliente/cliente.php';
        require_once 'view/footerAdmin.php';
    }

    public function NuevoCliente() {
        require_once 'view/header.php';
        require_once 'view/cliente/nuevo-cliente.php';
        require_once 'view/footer.php';
    }

    public function IngresarCliente() {
        require_once 'view/header.php';
        require_once 'view/cliente/registro-cliente.php';
        require_once 'view/footer.php';
    }

    public function ValidarCliente() {
        require_once 'view/header.php';
        require_once 'view/cliente/validar-cliente.php';
        require_once 'view/footer.php';
    }

    public function Validar() {
        $cliente = new Cliente();
        $cliente->idCliente = $_REQUEST['idCliente'];
        $var = $this->model->Obtener($_REQUEST['idCliente']);
        if ($var){
        $this->Mostrar($cliente->idCliente);
        }  else {
            echo "<script language='JavaScript'>alert('No hay ningun Cliente Registrado con ese DNI');</script>";
            $this->ValidarCliente();
        }
    }

    public function Mostrar($id) {
        $cliente = new Cliente();
        $cliente = $this->model->Obtener($id);
        if (isset($cliente)) {
            
            require_once 'view/header.php';
            require_once 'view/cliente/registro-cliente.php';
            require_once 'view/footer.php';
        } else {
            echo "<script language='JavaScript'>alert('No hay ningun Cliente Registrado con ese DNI');</script>";
        }
    }
    
    public function Guardar(){
        $cli = new Cliente();
        
        $cli->idCliente = $_REQUEST['idCliente'];
        $cli->nombre = $_REQUEST['nombre'];
        $cli->apellido = $_REQUEST['apellido'];
        $cli->telefono = $_REQUEST['telefono'];
        $cli->correo = $_REQUEST['correo'];
            
        $resultado = $this->model->RegistrarCliente($cli);
            if($resultado == 1)
            {
                echo '<script>alert("El Usuario se ha registrado con exito")</script>';
                $this->reservadmin->idCliente($cli);
                $this->reservadmin->ReservaCliente();
            }
            else
            {
                echo '<script>alert("ERROR, El Usuario no ha podido ser registrado")</script>';
            }
    }
    
    public function Actualizar(){
        
    }

}
