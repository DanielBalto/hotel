<?php

include_once 'model/ModelEmpleado.php';

class EmpleadoController {

    private $model;

    public function __CONSTRUCT() {
        $this->model = new Empleado();
    }

    public function Index() {

        session_start();
        error_reporting(0);
        
        $sesion = $_SESSION['user'];
        
        
        if(!$sesion){
            
            echo 'Usted no tiene autorizacion';
            die();
        } else {
            require_once 'view/headerAdmin.php';
            require_once 'view/usuario/empleado.php';
            require_once 'view/footerAdmin.php';
        }
    }

    public function Editar() {
        $usua = new Empleado();

        if (isset($_REQUEST['id'])) {
            $usua = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/usuario/empleado-editar.php';
        require_once 'view/footerAdmin.php';
    }

    public function Registrar() {
        $usua = new Empleado();

        if (isset($_REQUEST['idEmpleado'])) {
            $usua = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/headerAdmin.php';
        require_once 'view/usuario/empleado-editar.php';
        require_once 'view/footer.php';
    }

    public function RegistrarCl() {
        $usua = new Empleado();

        if (isset($_REQUEST['idEmpleado'])) {
            $usua = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'view/header.php';
        require_once 'view/usuario/empleado-editarCl.php';
        require_once 'view/footer.php';
    }

    public function Guardar() {
        $usua = new Empleado();

        $usua->idEmpleado = $_REQUEST['id'];
        $usua->tipo = $_REQUEST['Tipo'];
        $usua->nombre = $_REQUEST['Nombre'];
        $usua->apellido = $_REQUEST['Apellido'];
        $usua->login = $_REQUEST['NombreUsuario'];
        $usua->password = $_REQUEST['Clave'];
        $usua->Registrar($usua);

        header('Location: index.php?c=Empleado');
    }

    public function Eliminar() {
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php?c=Empleado');
    }

}
