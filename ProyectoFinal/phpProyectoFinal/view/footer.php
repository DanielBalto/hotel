
<div class="row">
    <div class="col-xs-12">
        <hr />
        <footer class="text-center well">
    
            <div id="infoCont">
                <h6 ><br><img src="img/telefono.png" alt=""/> +506 2664 1234</h6>
                <h6 ><br><img src="img/sobre-grande.png" alt=""/> info@midamahotel.com</h6>
                <h6 ><br><img src="img/home-s.png" alt=""/> Golfo Papagayo, Guanacaste, Costa Rica</h6>
            </div>
            <div id="derechos"><h3>Todos los derechos reservados &copy; 2018</h3></div>
        </footer>
    </div>
</div>

<script src="JS/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="JS/bootstrap.min.js"></script>
<script src="JS/jquery-ui/jquery-ui.min.js"></script>
<script src="JS/ini.js"></script>
<script src="JS/jquery.validator.js"></script>
</body>
</html>