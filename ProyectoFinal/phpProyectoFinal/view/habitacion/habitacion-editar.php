<h1 class="page-header">
    
</h1>

<ol class="breadcrumb">
    <li><a href="?c=Habitacion">Habitaciones</a></li>
    <li class="active"></li>
</ol>


<form id="frm-habitacion" action="?c=Habitacion&a=Guardar" method="post" enctype="multipart/form-data">
    
    <div class="form-group">
        <label>Numero de Habitacion</label>
        <input name="NumHab" value="" class="form-control" placeholder="Numero de Habitacion" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group">
        <label>Costo ($)</label>
        <input type="text" name="Costo" value="" class="form-control" placeholder="Ingrese el costo por noche" data-validacion-tipo="requerido|min:3" />
    </div>
    
        <div class="form-group">
        <label>Capacidad</label>
        <input type="number" min="1" name="Capacidad" value="" class="form-control" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group">
        <label>Estado</label>
        <select name="Estado" class="form-control" placeholder="Seleccione un estado Incial" data-validacion-tipo="requerido">
  <option value="Disponible">Disponible</option>
  <option value="Ocupada">Ocupada</option>
</select >
    </div>
    
    <div class="form-group">
        <label>Estilo</label>
        <input type="text" name="Estilo" value="" class="form-control" placeholder="Ingrese un estilo" data-validacion-tipo="requerido|min:4" />
    </div>
        <hr />

        <div class="text-right">
            <button class="btn-success">Guardar</button>
        </div>
</form>

<script>
    $(document).ready(function () {
        $("#frm-habitacion").submit(function () {
            return $(this).validate();
        });
    })
</script