<h1 class="page-header">Habitaciones</h1>
<form action="?c=Habitacion" method="post">

    <div class="well well-sm text-right">

        <div  style=" float: left; width:300px;">
            <label style=" float: left; height: 60px; margin-top: 7px; margin-right: 7px">Buscar:</label>
            <input class="form-control" id="buscar" type="text"  placeholder="Escriba algo para buscar" style="width:230px;" />
        </div>

        <a class="btn btn-primary" href="?c=Habitacion&a=Registrar">Registrar</a>
        <input type="submit" value="Editar" name="a" class="btn btn-primary"/>
        <input type="submit" value="Eliminar" name="a" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" class="btn btn-primary"/>
    </div>

    <table id="tabla" class="table table-striped">
        <thead>
            <tr>
                <th style="width:70px;"></th>
                <th style="width:150px;">Numero Habitacion</th>
                <th style="width:230px;">Costo</th>
                <th style="width:300px;">Capacidad</th>
                <th style="width:120px;">Estado</th>
                <th style="width:120px;">Estilo</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->model->ListarHabitacion() as $serv): ?>
                <?php $valor = $serv->idHabitacion; ?>
                <tr>
                    <td><input type=radio name=id value=<?php echo $serv->idHabitacion; ?> ></td>
                    <td><?php echo $serv->idHabitacion; ?></td>
                    <td><?php echo $serv->costo; ?></td>
                    <td><?php echo $serv->capacidad; ?></td>
                    <td><?php echo $serv->estado; ?></td>
                    <td><?php echo $serv->descripcion; ?></td>
                </tr>
            <?php endforeach; ?>
        <script src="assets/js/buscador.js"></script>
        </tbody>
    </table> 

</form>


