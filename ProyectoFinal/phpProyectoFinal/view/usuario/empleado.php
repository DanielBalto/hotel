<h1 class="page-header">Empleado</h1>
<form action="?c=Empleado" method="post">

    <div class="well well-sm text-right">

        <div  style=" float: left; width:300px;">
            <label style=" float: left; height: 60px; margin-top: 7px; margin-right: 7px">Buscar:</label>
            <input class="form-control" id="buscar" type="text"  placeholder="Escriba algo para buscar" style="width:230px;" />
        </div>

        <a class="btn btn-primary" href="?c=Empleado&a=Registrar">Registrar</a>
        <input type="submit" value="Editar" name="a" class="btn btn-primary"/>
        <input type="submit" value="Eliminar" name="a" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" class="btn btn-primary"/>
    </div>

    <table id="tablaMuestra"  class="table table-striped" style="border-collapse: collapse">
        <thead>
            <tr>
                <th style="width:150px;"></th>
                <th style="width:150px;">Cedula</th>
                <th style="width:70px;">Tipo</th>
                <th style="width:230px;">Nombre Usuario</th>
                <th style="width:230px;">Nombre</th>
                <th style="width:230px;">Apellido</th>
                

            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->model->Listar() as $serv): ?>
                <?php $valor = $serv->idEmpleado; ?>
                <tr>
                    <td><input type=radio name=id value=<?php echo $serv->idEmpleado; ?> ></td>
                    <td><?php echo $serv->idEmpleado; ?></td>
                    <td><?php echo $serv->tipo; ?></td>
                    <td><?php echo $serv->login; ?></td>
                    <td><?php echo $serv->nombre; ?></td>
                    <td><?php echo $serv->apellido; ?></td>
                    

                </tr>
            <?php endforeach; ?>
        <script src="assets/js/buscador.js"></script>
        </tbody>
    </table> 

</form>