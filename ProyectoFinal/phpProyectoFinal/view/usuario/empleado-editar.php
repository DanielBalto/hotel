<h1 class="page-header">
    
</h1>

<ol class="breadcrumb">
    <li><a href="?c=Empleado">Empleados</a></li>
    <li class="active"></li>
</ol>


<form id="frm-usuario" action="?c=Empleado&a=Guardar" method="post" enctype="multipart/form-data">
    
    <div class="form-group">
        <label>Cedula</label>
        <input name="id" value="" class="form-control" placeholder="Numero de cedula" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group">
        <label>Estado</label>
        <select name="Tipo" class="form-control" placeholder="Seleccione un estado Incial" data-validacion-tipo="requerido">
            <option value="Super Admin">Super Admin</option>
            <option value="Admministrador">Administrador</option>
            <option value="Usuario">Usuario</option>
        </select >
    </div>
    
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="Nombre" value="" class="form-control" placeholder="Ingrese un nombre de usuario" data-validacion-tipo="requerido|min:3" />
    </div>
    
        <div class="form-group">
        <label>Apellido</label>
        <input type="text" name="Apellido" value="" class="form-control" placeholder="Ingrese un nombre de usuario" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group">
        <label>Nombre Usuario</label>
        <input type="text" name="NombreUsuario" value="" class="form-control" placeholder="Ingrese un nombre de usuario" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group">
        <label>Contraseña</label>
        <input type="password" name="Clave" value="" class="form-control" placeholder="Ingrese su clave" data-validacion-tipo="requerido|min:4" />
    </div>
        <hr />

        <div class="text-right">
            <button class="btn-success">Guardar</button>
        </div>
</form>

<script>
    $(document).ready(function () {
        $("#frm-usuario").submit(function () {
            return $(this).validate();
        });
    })
</script