<h1><center>Login</center></h1>
<?php
if (isset($_GET['error'])) {
    echo "<script language='JavaScript'>alert('El Usuario o Contraseña ingresados son incorrectos."
    . " Intente Nuevamente');</script>";
}
?>

<form id="frm-login" action="?c=Login&a=Autenticar" method="post" enctype="multipart/form-data">



    <div class="form-group">
        <label>Nombre de Usuario</label>
        <input type="text" name="NombreUsuario" value="" class="form-control" placeholder="Ingrese un nombre de usuario" data-validacion-tipo="requerido|min:3" />
    </div>

    <div class="form-group">
        <label>Clave</label>
        <input type="password" name="Clave" value="" class="form-control" placeholder="Ingrese su clave" data-validacion-tipo="requerido|min:4" />
    </div>



    <div class="text-right">
        <button class="btn btn-success">INICIAR SESION</button>
    </div>


</form>

<script>
    $(document).ready(function () {
        $("#frm-login").submit(function () {
            return $(this).validate();
        });
    })
</script>






