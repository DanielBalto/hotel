<div id="botTopIndent">
<h1 class="page-header">
    <ol class="breadcrumb">
       <h4>Detalle de la Factura</h4>
<li class="active"></li>
    </ol>
</h1>
    </div>

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div id="botTopIndent" class="panel-body">
                    <form role="form" id="" action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                            </div>
                        </div>
                                   <label>Id</label>
                                       <input readonly="true" type="number" name="idDetalleFactura" value="<?php echo $this->factura->idDetalleFactura; ?>"  />
                                   <label>Número de la Factura</label>
                                            <input readonly="true" type="number" name="idFactura" value="<?php echo $this->factura->idFactura; ?>"  />
                                   <label>Número del detalle de la Reserva</label>
                                        <input readonly="true" type="number" name="idDetalleReserva" value="<?php echo $this->Reserva->idDetalleReserva; ?>"  />
                                   <label>SubTotal</label>
                                        <input readonly="true" type="number" name="subTotal" value="<?php echo $this->factura->subTotal; ?>"  />
                                    <label>Impuestos</label>
                                        <input readonly="true" type="number" name="impuestos" value="<?php echo $this->factura->impuestos; ?>"  />
                                    <label>Precio Total</label>
                                        <input readonly="true" type="number" name="precioTotal" value="<?php echo $this->factura->precioTotal; ?>"  />                   
                </div>
		</div>
            
            <div id="botTopIndent" class="text-right">
                <button class="btn-success">Siguiente</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>


<script src="JS/Galeria.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $("#frm-detalleFactura").submit(function () {
            return $(this).validate();
        });
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

</script>