
<h1 class="page-header">
    <ol class="breadcrumb">
        <h4>Registro Cliente</h4>
        <li class="active"></li>
    </ol>
</h1>

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Bienvenido!!!</h2>
                </div>
                <div id="botTopIndent" class="panel-body">
                    <form role="form" id="frm-nuevo-cliente" action="?c=Cliente&a=Guardar" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="idCliente"  class="form-control input-sm" placeholder="Escriba su DNI" required>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="nombre"   class="form-control input-sm" placeholder="Escriba su nombre" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="apellido"  class="form-control input-sm" placeholder="Escriba su apelllido" required>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="telefono" class="form-control input-sm" placeholder="Escriba su telefono" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="email" name="correo" class="form-control input-sm" placeholder="Escriba su correo" required>
                        </div>


                        <div id="botTopIndentBig" class="text-right">
                            <button class="btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#frm-nuevo-cliente").submit(function () {
            return $(this).validate();
        });
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

</script>

