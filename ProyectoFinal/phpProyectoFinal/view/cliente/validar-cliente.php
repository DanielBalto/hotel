
<div id="botTopIndent">
<h1 class="page-header">
    <ol class="breadcrumb">
        <h4>Validar Cliente</h4>
        <li class="active"></li>
    </ol>
</h1>
    </div>

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Bienvenido!!!</h2>
                </div>
                <div id="botTopIndent" class="panel-body">
                    <form role="form" id="frm-validar-cliente" action="?c=Cliente&a=Validar" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="idCliente" class="form-control input-sm" placeholder="Escriba su DNI" required>
                                </div>
                            </div>
                        </div>
                        <p> No eres cliente?  <a href="?c=Cliente&a=NuevoCliente">REGISTRATE</a> </p>
                </div>

            </div>
            <div id="topIndent" class="container">
            <div class="item">
                <img src="img/small/1.jpg" alt="" class="imagen" data="img/large/1.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/2.jpg" alt="" class="imagen" data="img/large/2.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/3.jpg" alt="" class="imagen" data="img/large/3.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/4.jpg" alt="" class="imagen" data="img/large/4.jpg" id="imagen">
            </div>
        </div>
            
            <div id="botTopIndent" class="text-right">
                <button class="btn-success">Siguiente</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>


<script src="JS/Galeria.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $("#frm-validar-cliente").submit(function () {
            return $(this).validate();
        });
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

</script>
