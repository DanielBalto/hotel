<div class="container">    
    <h1 class="page-header text-center">
        Registro de Servicios
    </h1>

    <form id="frm-habitacio-servivio" action="?c=HabitacionServicio&a=Guardar" method="post" enctype="multipart/form-data">
        <!-- Form Title -->
        <div class="form-heading text-center">
            <div class="title">Formulario para servivios y habitaciones</div>
        </div>
        <br>
        <div class="form-control">
            <h2>
                <select id="">
                    <option value="0" selected="selected" disabled="true">Habitaciones</option>
                    <?php foreach ($this->model->Descripcion() as $hab): ?>
                        <?php $valor = $hab->idHabitacion; ?>
                        <option value="<?php echo $hab->idHabitacion ?>" required><?php echo $hab->descripcion ?></option>
                    <?php endforeach; ?>

                </select>
            </h2>
        </div>
        <div class="col-md-2">
            <label>Nombre</label>
            <input type="text" name="nombre" required/> 
        </div>
        <div class="form-group">
            <label for="nombre">Nombre:</label>
            <input class="form-control" id="nombre" type="text" name="nombre"/>
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input class="form-control"  id="email" type="text" name="email"/>
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Recordarme
                </label>
            </div>
        </div>

        <button type="submit" class="btn btn-default">Enviar</button>

    </form>
    <br>
    <br>
    <br>
    <br>
</div>

<script>
    $(document).ready(function () {
        $("#frm-ingresar-cliente").submit(function () {
            return $(this).validate();
        });
    });

</script>
