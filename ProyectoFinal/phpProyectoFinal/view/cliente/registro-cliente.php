
<h1 class="page-header">
    <ol class="breadcrumb">
        <h4>Datos del Cliente</h4>
        <li class="active"></li>
    </ol>
</h1>

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Bienvenido!!!</h3>
                </div>
                <div class="panel-body">
                    <form role="form" id="frm-ingresar-cliente" action="?c=ReservAdmin&a=ReservaCliente" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input readonly type="text" name="idCliente"  value="<?php echo $cliente->idCliente; ?>" class="form-control input-sm" placeholder="Escriba su DNI" required>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="nombre" value="<?php echo $cliente->nombre; ?>"  class="form-control input-sm" placeholder="Escriba su nombre" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="apellido" value="<?php echo $cliente->apellido; ?>" class="form-control input-sm" placeholder="Escriba su apelllido" required>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="telefono" value="<?php echo $cliente->telefono; ?>" class="form-control input-sm" placeholder="Escriba su telefono" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="email" name="correo" value="<?php echo $cliente->correo; ?>" class="form-control input-sm" placeholder="Escriba su correo" required>
                        </div>

                        <div class="text-right">
                            <button class="btn-success">Crear Reserva</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#frm-ingresar-cliente").submit(function () {
            return $(this).validate();
        });
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

</script>

