<div class="social">
            <ul>
                <li><a href="http://www.facebook.com" target="_blank" class="icon-facebook-squared"></a></li>
                <li><a href="http://www.googleplus.com" target="_blank" class="icon-gplus"></a></li>
                <li><a href="http://www.twitter" target="_blank" class="icon-twitter-circled"></a></li>
                <li><a href="http://www.youtube.com" target="_blank" class="icon-youtube"></a></li>
                <li><a href="https://www.google.es/maps/dir//Culebra,+Provincia+de+Guanacaste,+Costa+Rica/@10.6425152,-85.6738833,15z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8f75810f5a8f2829:0xe7aee561f52ce922!2m2!1d-85.6499096!2d10.6473305" target="_blank" class="icon-location"></a></li>
            </ul>
        </div>

        <aside id="slideFotografias">
            <div class="slider-wrapper theme-bar">
                <div id="slider" class="nivoSlider">     
                    <img src="img/slide1.jpg" alt=""/>
                    <img src="img/slide2.jpg" alt=""/>
                    <img src="img/slide3.jpg" alt=""/>
                    <img src="img/slide4.jpg" alt=""/>
                    <img src="img/slide5.jpg" alt=""/>   
                </div> 
            </div>
        </aside> 

        <div id="Nota">
            <p>Click en las imagenes para ampliarlas</p>
        </div>

        <div class="container">
            <div class="item">
                <img src="img/small/1.jpg" alt="" class="imagen" data="img/large/1.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/2.jpg" alt="" class="imagen" data="img/large/2.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/3.jpg" alt="" class="imagen" data="img/large/3.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/4.jpg" alt="" class="imagen" data="img/large/4.jpg" id="imagen">
            </div>
        </div>

<script src="JS/Galeria.js" type="text/javascript"></script>
