  <aside id="slideFotografias">

            <div class="slider-wrapper theme-bar">
                <div id="slider" class="nivoSlider">     
                    <img src="img/slide6.jpg" alt=""/>
                    <img src="img/slide7.jpg" alt=""/>
                    <img src="img/slide8.jpg" alt=""/>
                    <img src="img/slide9.jpg" alt=""/>

                </div> 

                <!--<div id="htmlcaption" class="nivo-html-caption">     
                    <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. 
                </div> -->
            </div>
        </aside>

        <form action="" method="post">
            <h2>CONTACTENOS</h2>
            <input type="text" name="nombre" placeholder="Nombre" required>
            <input type="text" name="apellidos" placeholder="Apellidos" required>
            <input type="text" name="correo" placeholder="Correo" required>
            <input type="text" name="telefono" placeholder="Teléfono" required>
            <textarea name="mensaje" placeholder="Escriba aqui su mensaje" required></textarea>
            <input type="submit" value="ENVIAR" id="boton">
        </form>

        <div class="social">
            <ul>
                <li><a href="http://www.facebook.com" target="_blank" class="icon-facebook-squared"></a></li>
                <li><a href="http://www.googleplus.com" target="_blank" class="icon-gplus"></a></li>
                <li><a href="http://www.twitter" target="_blank" class="icon-twitter-circled"></a></li>
                <li><a href="http://www.youtube.com" target="_blank" class="icon-youtube"></a></li>
                <li><a href="https://www.google.es/maps/dir//Culebra,+Provincia+de+Guanacaste,+Costa+Rica/@10.6425152,-85.6738833,15z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8f75810f5a8f2829:0xe7aee561f52ce922!2m2!1d-85.6499096!2d10.6473305" target="_blank" class="icon-location"></a></li>
            </ul>
        </div>

        <div id="mapa">
            <br>
            <br>
            <h2> <center><stron>Ubiquenos en el mapa</stron></center></h2>
            <br>
            <br>

            <center> <iframe id="mapaVisual" src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d15684.751138414!2d-85.6738832521705!3d10.642515157460059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x8f75810f5a8f2829%3A0xe7aee561f52ce922!2sCulebra%2C+Provincia+de+Guanacaste%2C+Costa+Rica!3m2!1d10.647330499999999!2d-85.6499096!5e0!3m2!1ses!2ses!4v1493971604842" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </center>
                <br>
            <br>
        </div>
