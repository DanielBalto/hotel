<div class="container">
<link href="Css/estilos.css" rel="stylesheet" type="text/css"/>

<section>
    <div class="social">
        <ul>
            <li><a href="http://www.facebook.com" target="_blank" class="icon-facebook-squared"></a></li>
            <li><a href="http://www.googleplus.com" target="_blank" class="icon-gplus"></a></li>
            <li><a href="http://www.twitter" target="_blank" class="icon-twitter-circled"></a></li>
            <li><a href="http://www.youtube.com" target="_blank" class="icon-youtube"></a></li>
            <li><a href="https://www.google.es/maps/dir//Culebra,+Provincia+de+Guanacaste,+Costa+Rica/@10.6425152,-85.6738833,15z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8f75810f5a8f2829:0xe7aee561f52ce922!2m2!1d-85.6499096!2d10.6473305" target="_blank" class="icon-location"></a></li>
        </ul>
    </div>

    <aside id="slideFotografias">

        <div class="slider-wrapper theme-bar">
            <div id="slider" class="nivoSlider">     
                <img src="img/slide1.jpg" alt=""/>
                <img src="img/slide2.jpg" alt=""/>
                <img src="img/slide3.jpg" alt=""/>
                <img src="img/slide4.jpg" alt=""/>
                <img src="img/slide5.jpg" alt=""/>

            </div> 
        </div>
</section>

<article id="contenido">

    <form id="frm-cliente" action="?c=Reservas&a=Guardar" method="post" enctype="multipart/form-data">
        <div id="fecha">
            Entrada<input type="date" name="Entrada" data-validacion-tipo="requerido"> 
            Salida<input type="date" name="Salida" data-validacion-tipo="requerido">

        </div>

        <div id="personal">
            <h2 id="tituloPersonal">1. Datos Personales</h2>

            <label>DNI</label><input  type ="text" name="id" value="" class="form-control" placeholder="Nombre completo" data-validacion-tipo="requerido|min:3"> 
            <label>Nombre</label> <input  type ="text" name="Nombre" value="" class="form-control" placeholder="Nombre completo" data-validacion-tipo="requerido|min:3"> 
            <label>Apellidos</label><input  type ="text" name="Apellido" value="" class="form-control" placeholder="Indique los dos apellidos de ser necesario" data-validacion-tipo="requerido|min:3"> 
            <label>Telefono</label><input type ="text" name="Telefono" value="" class="form-control" placeholder="8888-8888" data-validacion-tipo="requerido|min:3">
            <label>Email</label><input type ="email" name="Correo" value="" class="form-control" placeholder="Se le enviara una confirmacion de la reserva" data-validacion-tipo="requerido| email">
            <label>Cantidad Adultos</label><input type="number" min="1" name="Adultos" value="" class="form-control" placeholder="MM" data-validacion-tipo="requerido|" />
            <label>Cantidad de Niños:</label><input type="number" min="0" name="Ninos" value="" class="form-control" placeholder="MM" data-validacion-tipo="requerido|" />
        </div>
        <div id="tarjeta">
            <h2 id="tituloTarjeta">2. Datos de la tarjeta</h2>
            <h3>Numero de  tarjeta</h3><input type ="text" name="NumeroTarjeta" value="" class="form-control" placeholder="8888-8888-8888-8888" data-validacion-tipo="requerido|min:4"> 
            <h3>Fecha Caducidad</h3> <input type="date" name="Vencimiento" value="" class="form-control" placeholder="MM" data-validacion-tipo="requerido|" />
        </div>
        <div id="observaciones">
            <h2 id="tituloObservacciones">3. Observaciones</h2>
            <h3>Observaciones(opcional)</h3><textarea id="observacion" name="Mensaje" value="" class="form-control" placeholder="escriba aqui su mensaje"> </textarea> 
        </div>
        <div class="text-right">
            <input type="submit" value="ENVIAR" id="boton">
        </div>

    </form>

    <table id="tabla" class="table table-striped">
        <thead>
            <tr>
                <th style="width:50px;"></th>
                <th style="width:30px;">Numero Habitacion</th>
                <th style="width:100px;">Costo</th>
                <th style="width:50px;">Capacidad</th>
                <th style="width:100px;">Estado</th>
                <th style="width:150px;">Estilo</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->model->ListarHabitacion() as $serv): ?>
                <?php $valor = $serv->idHabitacion; ?>
                <tr>
                    <td><input type=radio name=id value=<?php echo $serv->idHabitacion; ?> ></td>
                    <td><?php echo $serv->idHabitacion; ?></td>
                    <td><?php echo $serv->costo; ?></td>
                    <td><?php echo $serv->capacidad; ?></td>
                    <td><?php echo $serv->estado; ?></td>
                    <td><?php echo $serv->descripcion; ?></td>
                </tr>
            <?php endforeach; ?>
        <script src="assets/js/buscador.js"></script>
        </tbody>
    </table> 



    <script>
        $(document).ready(function () {
            $("#frm-cliente").submit(function () {
                return $(this).validate();
            });
            })
    </script>
</article>
</div>

