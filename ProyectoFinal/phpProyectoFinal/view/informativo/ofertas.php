<div class="social">
            <ul>
                <li><a href="http://www.facebook.com" target="_blank" class="icon-facebook-squared"></a></li>
                <li><a href="http://www.googleplus.com" target="_blank" class="icon-gplus"></a></li>
                <li><a href="http://www.twitter" target="_blank" class="icon-twitter-circled"></a></li>
                <li><a href="http://www.youtube.com" target="_blank" class="icon-youtube"></a></li>
                <li><a href="https://www.google.es/maps/dir//Culebra,+Provincia+de+Guanacaste,+Costa+Rica/@10.6425152,-85.6738833,15z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8f75810f5a8f2829:0xe7aee561f52ce922!2m2!1d-85.6499096!2d10.6473305" target="_blank" class="icon-location"></a></li>
            </ul>
        </div>

        <aside id="slideFotografias">

            <div class="slider-wrapper theme-bar">
                <div id="slider" class="nivoSlider">     
                    <img src="img/slide1.jpg" alt=""/>
                    <img src="img/slide2.jpg" alt=""/>
                    <img src="img/slide3.jpg" alt=""/>
                    <img src="img/slide4.jpg" alt=""/>
                    <img src="img/slide5.jpg" alt=""/>

                </div> 
                <!--<div id="htmlcaption" class="nivo-html-caption">     
                    <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. 
                </div> -->
            </div>
        </aside> 

        <article id="contenido">

            <div id="ContenidoOferta">
                <br>
                <h1><center>Aprovecha nuestras ofertas</center></h1>

                <br> <p><center>Consigue el mejor precio garantizado y disfruta de
                    ofertas y promociones especiales RESERVANDO EN NUESTA WEB.</center></p> <br>

                <aside class="tabla">
                    <div class="ofertas">
                        <div class="caja-precio">
                            <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                            <div><h4>En cualquier habitacion</h4></div>
                            <div class="precio-1"><span class="dolar">$</span>00<span class="mes"> X Noche</span></div>
                            <div class="foto">  <img src="img/oferta1.jpg "> </div> 
                            <div class="texto">

                                <h2>Piscina Gratuita</h2> <br>
                                <p> Le recordamos que todos nuestros huespedes tienen 
                                    derecho a la utilizacion SIN COSTO adicional para
                                    lograr hacer su estadia mas palcentera y divertida!!!</p>



                            </div>
                            <div class="reservar"><a href="index.php?c=Cliente&a=IndexClientValidar" class="reservar-a">RESERVAR</a></div>
                        </div>

                    </div>

                    <div class="ofertas">
                        <div class="caja-precio">
                            <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                            <div><h4>Habitacion Doble</h4></div>
                            <div class="precio-1"><span class="dolar">$</span>150<span class="mes"> X Noche</span></div>
                            <div class="foto">  <img src="img/oferta3.jpg "> </div> 
                            <div class="texto">

                                <h2>Comodidad Unica</h2> <br>
                                <p> 
                                    Nuestra habitación doble cuenta con suficiente
                                    espacio para que toda la familia este comoda!
                                    Asi como comodidad insuperable!</p>



                            </div>
                            <div class="reservar"><a href="index.php?c=Cliente&a=IndexClientValidar" class="reservar-a">RESERVAR</a></div>
                        </div>

                    </div>


                    <div class="ofertas">
                        <div class="caja-precio">
                            <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                            <div><h4>Desayuno Incluido</h4></div>
                            <div class="precio-1"><span class="dolar">$</span>10<span class="mes"> X Noche</span></div>
                            <div class="foto">  <img src="img/oferta2.jpg "> </div> 
                            <div class="texto">

                                <h2>Desayuno Continental</h2> <br>
                                <p> Por esta temporada todas las reservas cuentan
                                    con desayuno incluido por un costo adicional!
                                    No se vaya a perder esta deliciosa oferta!</p>
                            </div>
                            <div class="reservar"><a href="index.php?c=Cliente&a=IndexClientValidar" class="reservar-a">RESERVAR</a></div>
                        </div>

                    </div>

                    <div class="ofertas">
                        <div class="caja-precio">
                            <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                            <div><h4>24/7</h4></div>
                            <div class="precio-1"><span class="dolar">$</span>GRATIS<span class="mes"> ...</span></div>
                            <div class="foto">  <img src="img/oferta4.jpg "> </div> 
                            <div class="texto">

                                <h2>Atencion al cliente!</h2> <br>
                                <p> Recuerde que nuestras oficinas estan
                                    abiertas para hacer check-in las 24 horas
                                    los 7 dias de la semana! Sin ningun costo
                                adicional por late check-in</p>
                            </div>
                            <div class="reservar"><a href="index.php?c=Cliente&a=IndexClientValidar" class="reservar-a">RESERVAR</a></div>
                        </div>

                    </div>

                    <div class="ofertas">
                        <div class="caja-precio">
                            <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                            <div><h4>Cercania a Disney World</h4></div>
                            <div class="precio-1"><span class="dolar">.</span>47 000<span class="mes"> km</span></div>
                            <div class="foto">  <img src="img/oferta5.jpg "> </div> 
                            <div class="texto">

                                <h2>Disney World</h2> <br>
                                <p> Recuerde que somos un excelente hotel para
                                    que se hospede en su camino a Disney World
                                    ya que estamos solo a unos 47 000 km
                                    de este maravilloso parque familiar!</p>
                            </div>
                            <div class="reservar"><a href="index.php?c=Cliente&a=IndexClientValidar" class="reservar-a">RESERVAR</a></div>
                        </div>

                    </div>

                    <div class="ofertas">
                        <div class="caja-precio">
                            <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                            <div><h4>Habitacion Matrimonial</h4></div>
                            <div class="precio-1"><span class="dolar">$</span>120<span class="mes"> X Noche</span></div>
                            <div class="foto">  <img src="img/oferta6.jpg "> </div> 
                            <div class="texto">

                                <h2>Cama King</h2> <br>
                                <p> Todas nuestras habitaciones con camas
                                    matrimoniales cuentan con un tamaño king!
                                    Por que nos interesa asegurarnos que su comodidad
                                    sea absoluta!</p>
                            </div>
                            <div class="reservar"><a href="index.php?c=Cliente&a=IndexClientValidar" class="reservar-a">RESERVAR</a></div>
                        </div>

                    </div>
                </aside>

            </div>

        </article>
        <div id="textoAdic">

            <p> <br>
            <br><center>No dude en pasar unas hermosas vacaciones en nuestro hotel de lujo con estas 
                increibles promociones</center>.</p> 
        <br>
        <br>
    </div>
