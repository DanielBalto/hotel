<aside id="slideFotografias">

            <div class="slider-wrapper theme-bar">
                <div id="slider" class="nivoSlider">     
                    <img src="img/slide6.jpg" alt=""/>
                    <img src="img/slide7.jpg" alt=""/>
                    <img src="img/slide8.jpg" alt=""/>
                    <img src="img/slide9.jpg" alt=""/>

                </div> 

                <!--<div id="htmlcaption" class="nivo-html-caption">     
                    <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. 
                </div> -->
            </div>
        </aside>

        <div class="social">
            <ul>
                <li><a href="http://www.facebook.com" target="_blank" class="icon-facebook-squared"></a></li>
                <li><a href="http://www.googleplus.com" target="_blank" class="icon-gplus"></a></li>
                <li><a href="http://www.twitter" target="_blank" class="icon-twitter-circled"></a></li>
                <li><a href="http://www.youtube.com" target="_blank" class="icon-youtube"></a></li>
                <li><a href="https://www.google.es/maps/dir//Culebra,+Provincia+de+Guanacaste,+Costa+Rica/@10.6425152,-85.6738833,15z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8f75810f5a8f2829:0xe7aee561f52ce922!2m2!1d-85.6499096!2d10.6473305" target="_blank" class="icon-location"></a></li>
            </ul>
        </div>

        <div id="lujos">
            <div><p><img src="img/reception.png" alt=""/>Recepcion 24 horas</p></div>
            <div><p><img src="img/tv.png" alt=""/> Sala TV </p></div>
            <div><p><img src="img/wifi.png" alt=""/> Wifi Gratis </p></div>
            <div><p><img src="img/air-conditioner.png" alt=""/> Wifi Gratis </p></div>
            <div><p><img src="img/parking.png" alt=""/> Parqueo Privado </p></div>
            <div><p><img src="img/restaurant.png" alt=""/> Restaurante </p></div>
            <div><p><img src="img/swimming.png" alt=""/> Piscinas </p></div>



        </div>

        <div id="informacion">
            </br>
            </br>

            <h1>Nuestro Hotel</h1>
            <br>
            <br>

            <p>El MIDAMA Hotel es un hotel de nueva construcción ubicado
                en el hermoso Golfo Papagayo en  una de las playas más
                extensas del Golfo. <strong> Cuenta con mas de 100 habitaciones </strong> 
                todas ellas cuentan con terraza y la mayoría tienen vistas al mar.</p> <br>

            <p>Un hotel <strong>para disfrutarlo con niños.</strong> 
                Contamos con todas las instalaciones 
                que toda familia busca en un hotel: 
                Habitaciones amplias, camas grandes, posibilidad de
                reservar habitaciones cuádruples, tres piscinas (una de ellas infantil) ,
                animación infantil, entre otras cosas. No olvide consultar nuestras ofertas
                en las que incluimos los niños gratis</p> <br>





        </div>

        <div id="Nota">
            <p>Click en las imagenes para ampliarlas</p>
        </div>

        <div class="container">
            <div class="item">
                <img src="img/small/1.jpg" alt="" class="imagen" data="img/large/1.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/2.jpg" alt="" class="imagen" data="img/large/2.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/3.jpg" alt="" class="imagen" data="img/large/3.jpg" id="imagen">
            </div>
            <div class="item">
                <img src="img/small/4.jpg" alt="" class="imagen" data="img/large/4.jpg" id="imagen">
            </div>
        </div>