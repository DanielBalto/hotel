<section>

            <div class="social">
                <ul>
                    <li><a href="http://www.facebook.com" target="_blank" class="icon-facebook-squared"></a></li>
                    <li><a href="http://www.googleplus.com" target="_blank" class="icon-gplus"></a></li>
                    <li><a href="http://www.twitter" target="_blank" class="icon-twitter-circled"></a></li>
                    <li><a href="http://www.youtube.com" target="_blank" class="icon-youtube"></a></li>
                    <li><a href="https://www.google.es/maps/dir//Culebra,+Provincia+de+Guanacaste,+Costa+Rica/@10.6425152,-85.6738833,15z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8f75810f5a8f2829:0xe7aee561f52ce922!2m2!1d-85.6499096!2d10.6473305" target="_blank" class="icon-location"></a></li>
                </ul>
            </div>

            <aside id="slideFotografias">

                <div class="slider-wrapper theme-bar">
                    <div id="slider" class="nivoSlider">     
                        <img src="img/slide1.jpg" alt=""/>
                        <img src="img/slide2.jpg" alt=""/>
                        <img src="img/slide3.jpg" alt=""/>
                        <img src="img/slide4.jpg" alt=""/>
                        <img src="img/slide5.jpg" alt=""/>

                    </div> 
                    <!--<div id="htmlcaption" class="nivo-html-caption">     
                        <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. 
                    </div> -->
                </div>
            </aside> 

            <article id="contenido">

                <div id="ContenidoOferta">
                    <br>
                    <h1><center>Aprovecha nuestras ofertas</center></h1>

                    <br> <p><center>Consigue el mejor precio garantizado y disfruta de
                        ofertas y promociones especiales RESERVANDO EN NUESTA WEB.</center></p> <br>

                    <aside class="tabla">
                        <div class="ofertas">
                            <div class="caja-precio">
                                <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                                <div><h4>En cualquier habitacion</h4></div>
                                <div class="precio-1"><span class="dolar">$</span>80<span class="mes"> X Noche</span></div>
                                <div class="foto">  <img src="img/oferta1.jpg "> </div> 
                                <div class="texto">

                                    <h2>10% DTO. en NUESTRA WEB</h2> <br>
                                    <p> Reserve a través de nuestra web y obtenga el mejor precio
                                        garantizado con un descuento especial del 10% sobre precio 
                                        de tarifa</p>



                                </div>
                                <div class="reservar"><a href="?c=Cliente&a=ValidarCliente" class="reservar-a">RESERVAR</a></div>
                            </div>

                        </div>

                        <div class="ofertas">
                            <div class="caja-precio">
                                <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                                <div><h4>Habitacion Triple</h4></div>
                                <div class="precio-1"><span class="dolar">$</span>175<span class="mes"> X Noche</span></div>
                                <div class="foto">  <img src="img/oferta3.jpg "> </div> 
                                <div class="texto">

                                    <h2>Primer niño GRATIS</h2> <br>
                                    <p> GRATIS en base a régimen en Sólo Alojamiento,
                                        niños de 3 a 11 años. Oferta No Acumulable y
                                        sujeta a disponibilidad en temporada alta</p>



                                </div>
                                <div class="reservar"><a href="?c=Cliente&a=ValidarCliente" class="reservar-a">RESERVAR</a></div>
                            </div>

                        </div>


                        <div class="ofertas">
                            <div class="caja-precio">
                                <div class="icono-precio"><i class="fa fa-cubes" aria-hidden="true"></i></div>
                                <div><h4>Desayuno Incluido</h4></div>
                                <div class="precio-1"><span class="dolar">$</span>120<span class="mes"> X Noche</span></div>
                                <div class="foto">  <img src="img/oferta2.jpg "> </div> 
                                <div class="texto">

                                    <h2>GRATIS desayuno</h2> <br>
                                    <p> Reserve uan habitacion doble y obtenga el
                                        beneficio del desayunno GRATIS al estilo buffete
                                        en cualquiera de nuestros restaurantes del hotel</p>



                                </div>
                                <div class="reservar"><a href="?c=Cliente&a=ValidarCliente" class="reservar-a">RESERVAR</a></div>
                            </div>

                        </div>




                    </aside>

                </div>

            </article>
            <div id="textoAdic">
                <br>
                <br>
                <p> <center>Disfrute de nuestras habitaciones con vistas despejadas al 
                    Océano Pacífico. A unos pasos de la playa, desde la terraza
                    de su habitación, los huéspedes pueden descansar de un día 
                    inolvidable mientras disfrutan de los coloridos atardeceres
                    que ofrece el Oceáno Pacífico.</center></p>
                <br>
                <br>
            </div>
        </section>


