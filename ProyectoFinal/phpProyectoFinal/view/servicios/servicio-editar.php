<h1 class="page-header">

</h1>

<ol class="breadcrumb">
    <li><a href="?c=Servicio">Servicios</a></li>
    <li class="active"></li>
</ol>


<form id="frm-servicio" action="?c=Servicio&a=Guardar" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label>Descripcion</label>
        <input name="Descripcion" value="" class="form-control" placeholder="Nombre del servicio" data-validacion-tipo="requerido|min:3" />
    </div>

    <div class="form-group">
        <label>Costo ($)</label>
        <input type="text" name="Costo" value="" class="form-control" placeholder="Ingrese el costo" data-validacion-tipo="requerido|min:3" />
    </div>

    <div class="form-group">
        <label>Estado</label>
        <select name="Estado" class="form-control" placeholder="Seleccione un estado Incial" data-validacion-tipo="requerido">
            <option value="Disponible">Disponible</option>
            <option value="No Disponible">No Disponible</option>
        </select >
    </div>

    <div class="text-right">
        <button class="btn-success">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $("#frm-servicio").submit(function () {
            return $(this).validate();
        });
    })
</script