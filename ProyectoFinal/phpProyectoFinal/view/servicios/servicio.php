<h1 class="page-header">Servicios</h1>
<form action="?c=Servicio" method="post">

    <div class="well well-sm text-right">

        <div  style=" float: left; width:300px;">
            <label style=" float: left; height: 60px; margin-top: 7px; margin-right: 7px">Buscar:</label>
            <input class="form-control" id="buscar" type="text"  placeholder="Escriba algo para buscar" style="width:230px;" />
        </div>

        <a class="btn btn-primary" href="?c=Servicio&a=Registrar">Registrar</a>
        <input type="submit" value="Editar" name="a" class="btn btn-primary"/>
        <input type="submit" value="Eliminar" name="a" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" class="btn btn-primary"/>
    </div>

    <table id="tablaMuestra"  class="table table-striped" style="border-collapse: collapse">
        <thead>
            <tr>
                <th style="width:10px;"></th>
                <th style="width:50px;">Codigo</th>
                <th style="width:100px;">Descripcion</th>
                <th style="width:30px;">Costo</th>
                <th style="width:120px;">Estado</th>
         
            </tr>
        </thead>
        <tbody>
            <?php foreach ($this->model->Listar() as $serv): ?>
                <?php $valor = $serv->idServicio; ?>
                <tr>
                    <td><input type=radio name=id value=<?php echo $serv->idServicio; ?> ></td>
                    <td><?php echo $serv->idServicio; ?></td>
                    <td><?php echo $serv->descripcion; ?></td>
                    <td><?php echo $serv->costo; ?></td>
                    <td><?php echo $serv->estado; ?></td>
                                   
                </tr>
            <?php endforeach; ?>
        <script src="assets/js/buscador.js"></script>
        </tbody>
    </table> 

</form>