<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="Css/estiloSlider.css" rel="stylesheet" type="text/css"/>
<link href="Css/nivo-slider.css" rel="stylesheet" type="text/css"/>
<link href="themes/bar///bar.css" rel="stylesheet" type="text/css"/>
<link href="Css/fontello.css" rel="stylesheet" type="text/css"/>
<link id="pagestyle" rel="stylesheet" type="text/css" href="Css/estilos.css">
<link href="Css/galeria.css" rel="stylesheet" type="text/css"/>



<script src="JS/jquery.js" type="text/javascript"></script>
<script src="JS/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="JS/scriptSlider.js" type="text/javascript"></script>
<script src="JS/scriptMenu.js" type="text/javascript"></script>
<script src="JS/sizeController.js" type="text/javascript"></script>
<script src="JS/Galeria.js" type="text/javascript"></script>

<div class="row">
    <div id="divTotalReserva">
        <form class="text-center" id="frm-reserva" action="?c=Reservas&a=Guardar" method="post" enctype="multipart/form-data">
            <div class="panel-body">
                <!-- Form Title --> 


                <div class="panel panel-default">
                    <h1 class="page-header">
                        <ol class="breadcrumb">
                            <li><a href="?c=inicio">Ingresar Reserva</a></li>
                            <li class="active"></li>
                        </ol>
                    </h1>

                    <div id="botTopIndent" class="panel-body">
                        <div class="row">
                            <!-- Fecha y Numero de Reserva -->
                            <div class="row form-group text-center">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label>#Reserva</label>
                                    <input readonly="true" type="number" name="idReserva" value="<?php echo $this->reserva->idReserva; ?>"  /> 
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label>Fecha: </label>
                                    <input readonly="true" id="txtfecha" name="fechaPago" value="<?php echo date("d/m/y"); ?>"/>  
                                </div>  
                            </div>
                            <!-- Cedula del Cliente -->
                            <div class="row form-group text-center">

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label>Cedula: </label>
                                    <input type="text" name="idCliente" value="<?php echo $this->id->idCliente; ?>" readonly="true" /> 
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label>Nombre</label>
                                    <input type="text" name="nombre" value="<?php echo $this->id->nombre; ?>" readonly="true" /> 
                                </div>
                            </div>
                            <!-- Cantidad Ninos y Adultos  -->

                            <div class="row text-center">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <select class='form-control' required name="cantAdultos" >
                                        <option value=""selected="">--Cantidad de Adultos--</option>
                                        <option value="1">1</option>
                                        <option value="2" >2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <select class='form-control' required name="cantNinos" >
                                        <option value=""selected="">--Cantidad de Niños--</option>
                                        <option value="1">1</option>
                                        <option value="2" >2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                                <div id="topIndent" class="col-xs-6 col-sm-6 col-md-6">
                                    <label>Nº Tarjeta</label>
                                    <input type="text" name="numTarjeta"  required /> 
                                </div>
                                <div id="topIndent" class="col-xs-6 col-sm-6 col-md-6">
                                    <label>Fech/Venc</label>
                                    <input type="text" class="datepicker"  name="fechaCaduc" required/> 
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row text-center">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label>Fecha ingreso</label>
                                    <input type="text" class="datepicker" name="fechaIngreso" required /> 
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <label>Fecha salida</label>
                                    <input type="text" class="datepicker"  name="fechaSalida" required/> 
                                </div>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br>


            <div class="panel panel-default">
                <div class="row text-center">
                    <div class="panel-heading">
                        <h2 class="panel-title">Habitaciones</h2>
                    </div>
                    <br>
                    <div id="divtworeserva" class="panel-body">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="panel-heading">
                                <h2 class="panel-title">Pequeña</h2>
                            </div>
                            <div id="divtworeserva">
                                <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="3">Familiar</label>
                                <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="2">Personal</label>
                                <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="1">Suite Basica</label>
                                <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="7">Familiar Basica</label>
                            </div>
                        </div>
                        <div id="divtworeserva" class="col-xs-6 col-sm-6 col-md-6">
                            <div class="panel-heading">
                                <h2 class="panel-title">Mediana</h2>
                            </div>
                            <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="4">Senior Suite</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="5">Doble Sencilla</label> 
                            <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="8">Suite Jacuzzi</label>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="panel-heading">
                                <h2 class="panel-title">Grande</h2>
                            </div>
                            <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="6">Doble Estandar</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="9">Doble Superior</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habitaciones[]" value="10">Suite Familiar</label> 
                        </div>
                    </div>
                    <div>
                        <div class="panel-heading">
                            <h2 class="panel-title">Servicios</h2>
                        </div>
                        <br>
                        <div class="row text-center">
                            <div>
                                <label class="radio-inline"><input type="checkbox" id="check1" value="1" name="servicios[]" onchange="mostrar()" checked>Todo Incluido</label>
                            </div> 
                            <br>
                            <div>
                                <label class="radio-inline"><input type="checkbox" id="check2" value="2" name="servicios[]">Solo Desayuno</label>
                                <label class="radio-inline"><input type="checkbox" id="check3" value="3" name="servicios[]">Solo Almuerzo</label> 
                                <label class="radio-inline"><input type="checkbox" id="check4" value="4" name="servicios[]">Solo Cena</label>
                                <label class="radio-inline"><input type="checkbox" id="check5" value="5" name="servicios[]">Canopy</label> 
                                <label class="radio-inline"><input type="checkbox" id="check6" value="6" name="servicios[]">Tour Catamaran</label>
                                <label class="radio-inline"><input type="checkbox" id="check7" value="7" name="servicios[]">Spa</label> 
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <br>
            <div class="form-group-lg text-center">
                <button class="btn-success">Crear Reserva</button>  
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#frm-reserva").submit(function () {
            return $(this).validate();
        });
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

</script>
