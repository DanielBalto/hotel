<h1 class="page-header">

</h1>

<ol class="breadcrumb">
    <li><a href="?c=Usuario">Usuarios</a></li>
    <li class="active"></li>
</ol>


<form id="frm-estilo" action="?c=Estilo&a=Guardar" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label>Nombre del Estilo</label>
        <input name="Descripcion" value="" class="form-control" placeholder="Nombre del Estilo" data-validacion-tipo="requerido|min:3" />
    </div>

    <div class="text-right">
        <button class="btn-success">Guardar</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $("#frm-estilo").submit(function () {
            return $(this).validate();
        });
    })
</script