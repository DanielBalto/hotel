    <!DOCTYPE html>
    <html lang="es">
        <head>
            <title>ADMINISTRACION DE RESERVAS</title>

            <meta charset="utf-8" />

            <link rel="stylesheet" href="Css/bootstrap.min.css" />
            <link rel="stylesheet" href="Css/bootstrap-theme.min.css" />
            <link rel="stylesheet" href="JS/jquery-ui/jquery-ui.min.css" />
            <link rel="stylesheet" href="Css/style.css" />

            <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
        </head>
        <body>

            <div class="container">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Registros</a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li><a href="index.php?c=Cliente">Clientes</a></li>
                            <li><a href="index.php?c=Estilo">Estilos</a></li>
                            <li><a href="index.php?c=Habitacion">Habitaciones</a></li>
                            <li><a href="index.php?c=Servicio">Servicios</a></li>
                            <li><a href="index.php?c=Reservadmin">Reservas</a></li>
                            <li><a href="index.php?c=Empleado">Empleados</a></li>
                            <li><a href="?c=Restaurar&a=backup">Backup</a></li>
                            <li class="active"><a href="index.php?c=Inicio&a=CerrarSession">Cerrar Sesion</a></li>
                    </ul>
                </div>
            </nav>
                