<?php
include_once 'model/database.php';
class Mostrar{
    private $pdo;
    //public $idReserva;
    public $nombre;
    public $fechaIngreso;
    public $fechaSalida;
    public $idHabitacion;
    public $descripcion;
    public $Costo;
    public $capacidad;
    public $estado;






    public function __CONSTRUCT() {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function ListarReserva() {
        try {
            $result = array();

            $stm = $this->pdo->prepare("select re.idReserva, cl.nombre, 
re.fechaIngreso, re.fechaSalida, 
ha.idHabitacion, es.descripcion,
(datediff(re.fechaSalida,re.fechaIngreso)*ha.costo) + rs.costo as costo
from reserva as re inner join cliente as cl on re.idCliente = cl.idCliente
inner join detallereserva as dr on dr.idReserva = re.idReserva
inner join habitacion as ha on ha.idHabitacion = dr.idHabitacion
inner join estilo as es on es.idEstilo = ha.estilo
inner join reservaservicios as rs on rs.reservaID = re.idReserva");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function ListarHabitacion() {
        try {
            $result = array();

            $stm = $this->pdo->prepare("select idHabitacion, costo, capacidad, estado, descripcion
        from habitacion inner join estilo on habitacion.estilo = estilo.idEstilo");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
