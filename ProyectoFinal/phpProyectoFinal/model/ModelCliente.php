<?php

include_once 'model/database.php';

class Cliente {

    private $pdo;
    public $idCliente;
    public $nombre;
    public $apellido;
    public $telefono;
    public $correo;

    public function __CONSTRUCT() {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Listar() {
        try {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM cliente");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Obtener($id) {
        try {
            $stm = $this->pdo->prepare('{Call pa_obtenerID_cliente (?)}');
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
//arreglar SIN FUNCIONAMIENTO
    public function Validar($id) {
        try {
            $stm = $this->pdo
                    ->prepare('{Call pa_valCliente(?,?)}');
            
            $stm->execute(array($id));

            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

 
     // ELIMINAR Cliente POR ID ////////////////////////////////////////////////
    // LLAMADA AL PROCEDIMIENTO ALMACENADO: pa_eliminar_cliente
    public function Eliminar($id) 
    {
        try 
        {
            $resultado;
             
            $stm = $this->pdo->prepare('{Call pa_DelCliente(?)}');
            $resultado = $stm->execute(array($id));

            if ($resultado == 1) 
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }

    public function RegistrarCliente(Cliente $data) {

        try {
            $resultado;

            $stm = $this->pdo->prepare('{Call pa_insCliente(?,?,?,?,?)}');

            $resultado = $stm->execute(
                    array(
                        $data->idCliente,
                        $data->nombre,
                        $data->apellido,
                        $data->telefono,
                        $data->correo
                    )
            );
            if ($resultado == 1) {

                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function Actualizar($data) {
        try {
            $resultado;

            $stm = $this->pdo->prepare('{Call pa_actualizar_cliente(?,?,?,?,?)}');
            $resultado = $stm->execute(array($data->idCliente, $data->nombre, $data->apellido, $data->telefono, $data->correo));

            if ($resultado == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
