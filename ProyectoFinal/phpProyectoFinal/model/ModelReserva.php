<?php

include_once 'model/database.php';

class Reserva {

    private $pdo;
    // reserva
    public $idReserva;
    public $idCliente;
    public $fecha;
    public $cantAdultos;
    public $cantNinos;
    public $numTarjeta;
    public $fechaCaducTarj;
    public $estado;
    public $fechaIngreso;
    public $fechaSalida;
    public $subTotal;
    // detalle reserva.
    public $idDetalle;
    public $idHabitacion;
    public $idReservaDet;
    public $precioTotal;
    //reservaServ
    public $idReservaServicios;
    public $reservaIDServ;
    public $costoReserServ;

    public function __CONSTRUCT() {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function ObtenerIdReserva() {
        try {
            $consulta = $this->pdo->prepare('{Call pa_UltimoIdReserva}');

            $consulta->execute();
            $this->idReserva = $consulta->fetch(PDO::FETCH_COLUMN);
            return $this->idReserva;
        } catch (Exception $exc) {
            die($exc->getMessage());
        }
    }

    public function RegistrarReserva(Reserva $data) {
        try {
            $resultado;

            $stm = $this->pdo->prepare('{Call pa_insReserva(?,?,?,?,?,?,?,?,?,?)}');
            $resultado = $stm->
                    execute(array(
                $data->idCliente,
                $data->fecha,
                $data->cantAdultos,
                $data->cantNinos,
                $data->numTarjeta,
                $data->fechaCaducTarj,
                'pendiente',
                $data->fechaIngreso,
                $data->fechaSalida,
                0.0));

            if ($resultado == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function idReserva(Reserva $data) {
        try {
            $resultado;

            $stm = $this->pdo->prepare('{Call pa_insReserva(?,?,?,?,?,?,?,?,?,?)}');
            $resultado = $stm->
                    execute(array());

            if ($resultado == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function DetalleReserva(Reserva $data) {


        try {
            $resultado;

            $stm = $this->pdo->prepare('{Call pa_insDetalleReserva (?,?,?,?,?)}');
            $resultado = $stm->
                    execute(array($data->idReserva, $data->idHabitacion, 0.0, 0.0, 0.0));

            if ($resultado == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function ReservaServicio(Reserva $data){
        try {
            $result;
            $stm = $this->pdo->prepare('{Call pa_insReservaServicio(?,?,?)}');
            $result = $stm->execute(array($data->idReserva,$data->reservaIDServ,0.0));
            if($result == 1){
                return 1;
            }  else {
                return 0;
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        }

}
