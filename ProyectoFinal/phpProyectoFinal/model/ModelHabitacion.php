<?php

include_once 'model/database.php';
class Habitacion{
    
    private $pdo;
    public $idHabitacion;
    public $costo;
    public $capacidad;
    public $estado;
    public $descripcion;
    
     public function __CONSTRUCT() {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    

    public function Obtener($id) {
        try {
            $stm = $this->pdo
                    ->prepare("SELECT * FROM habitacion WHERE idHabitacion = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    // MOSTRAR TODOS LOS PRODUCTOS /////////////////////////////////////////////
    // LLAMADA AL PROCEDIMIENTO ALMACENADO: pa_obtener_productos
    public function Descripcion() 
    {
        try 
        {
            $stm = $this->pdo->prepare('SELECT * FROM vHabitacion');
            $stm->execute();

            return $stm->fetchall(PDO::FETCH_OBJ);
        } 
        catch (Exception $e) 
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id) {
        try {
            $stm = $this->pdo
                    ->prepare("DELETE FROM habitacion WHERE idHabitacion = ?");

            $stm->execute(array($id));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function Registrar(Habitacion $data) {

        try {
            $sql = "INSERT INTO habitacion (idHabitacion,costo,capacidad,estado,estilo) 
		        VALUES (?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                    ->execute(
                            array(
                                $data->idHabitacion,
                                $data->costo,
                                $data->capacidad,
                                $data->estado,
                                $data->estilo
                            )
            );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    

}

