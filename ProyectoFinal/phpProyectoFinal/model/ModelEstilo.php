<?php

include_once 'model/database.php';
class Estilo {

    private $pdo;
    public $idEstilo;
    public $descripcion;
   

    public function __CONSTRUCT() {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Listar() {
        try {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM estilo");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Obtener($id) {
        try {
            $stm = $this->pdo
                    ->prepare("SELECT * FROM estilo WHERE idEstilo = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Eliminar($id) {
        try {
            $stm = $this->pdo
                    ->prepare("DELETE FROM estilo WHERE idEstilo = ?");

            $stm->execute(array($id));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Registrar(Estilo $data) {

        try {
            $sql = "INSERT INTO estilo (descripcion) 
		        VALUES (?)";

            $this->pdo->prepare($sql)
                    ->execute(
                            array(
                              $data->descripcion
                            )
            );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
