<?php
include_once 'model/database.php';
class Servicio{
    
    private $pdo;
    
    public $idServicio;
    public $descripcion;
    public $costo;
    public $estado;
    
    public function __CONSTRUCT() {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Listar() {
        try {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM servicios");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Obtener($id) {
        try {
            $stm = $this->pdo
                    ->prepare("SELECT * FROM servicios WHERE idServicio = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Eliminar($id) {
        try {
            $stm = $this->pdo
                    ->prepare("DELETE FROM servicios WHERE idServicio = ?");
            $stm->execute(array($id));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
        public function Registrar(Servicio $data) {

        try {
            $sql = "INSERT INTO servicios (descripcion, costo, estado) 
		        VALUES (?, ?, ?)";

            $this->pdo->prepare($sql)
                    ->execute(
                            array(
                                $data->descripcion,
                                $data->costo,
                                $data->estado
                            )
            );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
     
    }

