<?php

include_once 'model/database.php';

class Empleado{
    
    public $pdo;
    public $idEmpleado;
    public $tipo;
    public $login;
    public $password;
    public $nombre;
    public $apellido;
    
     public function __CONSTRUCT() {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Listar() {
        try {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM empleado");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Obtener($id) {
        try {
            $stm = $this->pdo
                    ->prepare("SELECT * FROM empleado WHERE idEmpleado = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function Eliminar($id) {
        try {
            $stm = $this->pdo
                    ->prepare("DELETE FROM empleado WHERE idEmpleado = ?");
            $stm->execute(array($id));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
        public function Registrar(Empleado $data) {

        try {
            $sql = "INSERT INTO empleado (idEmpleado,tipo,login,password,nombre,apellido) 
		        VALUES (?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                    ->execute(
                            array(
                                $data->idEmpleado,
                                $data->tipo,
                                $data->login,
                                sha1($data->password),
                                $data->nombre,
                                $data->apellido
                            )
            );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
     public function Verificar($nombre, $password) {

        try {
            $sql = "SELECT  login, password FROM empleado WHERE login = ? AND password = ?";
            $stm = $this->pdo->prepare($sql);
            $stm->execute(array($nombre, $password));

            $usuarioDatos = $stm->fetch(PDO::FETCH_OBJ);
            if ($usuarioDatos == NULL) {
                return FALSE;
            } else {
                return TRUE;
            }
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }
    
    public function restaurar() {
        try {
            $resultado;

            $stm = $this->pdo->prepare('{Call pa_hotel_respaldo()}');
            $resultado = $stm->execute();
            if($resultado == 1){
                return 1;
                
                echo 'exitoso';
            }  else {
                echo 'error';
                return 0;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    
}

