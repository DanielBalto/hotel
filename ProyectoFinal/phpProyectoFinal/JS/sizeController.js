function layoutHandler(){
	var styleLink = document.getElementById("pagestyle");
	if(window.innerWidth < 900){
		styleLink.setAttribute("href", "Css/mobile.css");
	} else if(window.innerWidth < 1200){
		styleLink.setAttribute("href", "Css/medio.css");
	} else {
	    styleLink.setAttribute("href", "Css/estilos.css");
	}
}
window.onresize = layoutHandler;
layoutHandler();
